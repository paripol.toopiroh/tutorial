#include <iostream>

#include "hu.pb.h"
using namespace std;

int
main(int argc, char *argv[]) {

 GOOGLE_PROTOBUF_VERIFY_VERSION;

 HU::SensorEvent_GearData gear_pos;

 cout<<"set gear to 8"<<endl;
 gear_pos.set_gear(gear_pos.GEAR_EIGHTH);
 if (gear_pos.has_gear()) {
	cout<<"has gear : "<<gear_pos.gear()<<endl;
 }

 cout<<"set gear to D"<<endl;
 gear_pos.set_gear(gear_pos.GEAR_DRIVE);
 if (gear_pos.has_gear()) {
	cout<<"has gear : "<<gear_pos.gear()<<endl;
 }

 cout<<"has gear Name : "<<gear_pos.GEAR_Name(gear_pos.gear())<<endl;
 printf("OK\n ");
 return(0);
}
